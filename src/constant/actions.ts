// USER
export const USER_STORE: string = "USER_STORE";
export const GENDER_STORE: string = "GENDER_STORE";
export const USER_CLEAR: string = "USER_CLEAR";
export const USER_ROLL: string = "USER_ROLL";
// MONSTER
export const MONSTER_STORE: string = "MONSTER_STORE";
export const MONSTER_CLEAR: string = "MONSTER_CLEAR";
export const MONSTER_ROLL: string = "MONSTER_ROLL";
// GAME
export const GAME_NEW: string = "GAME_NEW";
export const GAME_RESET: string = "GAME_RESET";
export const GAME_ATTACK: string = "GAME_ATTACK";
export const GAME_DICE_ROLLING: string = "GAME_DICE_ROLLING";
