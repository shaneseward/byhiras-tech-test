import React from "react";
import styles from "./WelcomeStyles.module.scss";
import Player from "../../components/Player/Player";
import PlayerInput from "../../components/PlayerInput/PlayerInput";
import Monster from "../../components/Monster/Monster";

const Welcome = () => {
  return (
    <div className={styles.root}>
      <Player />
      <PlayerInput />
      <Monster />
    </div>
  );
};

export default Welcome;
