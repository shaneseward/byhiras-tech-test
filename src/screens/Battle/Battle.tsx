import React from "react";
import styles from "./BattleStyles.module.scss";
import Player from "../../components/Player/Player";
import Monster from "../../components/Monster/Monster";
import GameDetails from "../../components/GameDetails/GameDetails";

const Battle = () => {
  return (
    <div className={styles.root}>
      <Player />
      <GameDetails />
      <Monster />
    </div>
  );
};

export default Battle;
