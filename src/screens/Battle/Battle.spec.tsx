import React from "react";
import Battle from "./Battle";
import ShallowRenderer from "react-test-renderer/shallow";

/**
 * Example component snapshot (using shallow renderer)
 */

it("renders correctly", () => {
  const renderer = new ShallowRenderer();
  const result = renderer.render(<Battle />);
  expect(result).toMatchSnapshot();
});
