import React from "react";
import styles from "./GameOverStyles.module.scss";
import GameComplete from "../../components/GameComplete/GameComplete";

const GameOver = () => {
  return (
    <div className={styles.root}>
      <GameComplete />
    </div>
  );
};

export default GameOver;
