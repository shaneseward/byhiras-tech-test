import { createMuiTheme, Theme } from "@material-ui/core";

const darkTheme: Theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      // light: will be calculated from palette.primary.main,
      main: "#fe8423",
      // dark: will be calculated from palette.primary.main,
      contrastText: "#ffffff"
    },
    secondary: {
      // light: will be calculated from palette.primary.main,
      main: "#c5f616"
      // dark: will be calculated from palette.primary.main,
      //contrastText: '#ffffff'
    }
  },
  overrides: {
    MuiButtonBase: {
      root: {
        minHeight: 55
      }
    },
    MuiButton: {
      root: {
        fontSize: 23,
        fontFamily: '"NameFont", Arial, serif'
      }
    }
  }
});

export default darkTheme;
