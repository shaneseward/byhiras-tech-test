import configureStore from "redux-mock-store";
import {
  GAME_ATTACK,
  GAME_DICE_ROLLING,
  MONSTER_ROLL,
  USER_ROLL
} from "../../constant/actions";
import { gameMiddleware } from "./game";
import { AnyAction } from "redux";
import thunk from "redux-thunk";
import { ROLL_TIME } from "../../constant/game";

const mockStore = configureStore([thunk]);

/**
 * Examples of middleware tests (using redux-mock-store)
 */
describe("game middleware", () => {
  it("GAME_ATTACK should be handled correctly", done => {
    const initialState = {};
    const store = mockStore(initialState);
    const next = jest.fn();
    const action: AnyAction = {
      type: GAME_ATTACK
    };
    gameMiddleware(store)(next)(action);
    expect(store.getActions()[0].type).toEqual(GAME_DICE_ROLLING);
    expect(store.getActions()[0].payload.rolling).toEqual(true);
    setTimeout(() => {
      expect(store.getActions()[1].type).toEqual(USER_ROLL);
      expect(store.getActions()[2].type).toEqual(MONSTER_ROLL);
      expect(store.getActions()[3].type).toEqual(GAME_DICE_ROLLING);
      expect(store.getActions()[3].payload.rolling).toEqual(false);
      done();
    }, ROLL_TIME + 10);
  });
});
