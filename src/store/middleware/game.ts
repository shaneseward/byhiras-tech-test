import { GAME_ATTACK } from "../../constant/actions";
import { diceRolling } from "../action/game";
import { storeMonsterRoll, storeUserRoll } from "../action";
import { ROLL_TIME } from "../../constant/game";

export const gameMiddleware = (store: { dispatch: any }) => (next: any) => (
  action: any
) => {
  const { type } = action;

  if (type === GAME_ATTACK) {
    store.dispatch(diceRolling(true));
    const mosterRoll = [getRandomDiceNum(), getRandomDiceNum()];
    const userRoll = [getRandomDiceNum(), getRandomDiceNum()];
    const mosterTotal = mosterRoll[0] + mosterRoll[1];
    const userTotal = userRoll[0] + userRoll[1];
    const monsterWinnings =
      mosterTotal > userTotal ? mosterTotal - userTotal : 0;
    const userWinnings = userTotal > mosterTotal ? userTotal - mosterTotal : 0;

    setTimeout(() => {
      store.dispatch(storeUserRoll(userRoll, userWinnings));
      store.dispatch(storeMonsterRoll(mosterRoll, monsterWinnings));
      store.dispatch(diceRolling(false));
    }, ROLL_TIME);
  }

  return next(action);
};

const getRandomDiceNum = () => {
  return Math.floor(Math.random() * 6) + 1;
};
