import {
  GENDER_STORE,
  USER_CLEAR,
  USER_ROLL,
  USER_STORE
} from "../../constant/actions";
import { Action } from "redux";

export interface StoreUserAction extends Action<string> {
  payload: {
    username: string;
  };
}

export interface StoreGenderAction extends Action<string> {
  payload: {
    gender: string;
  };
}

export interface StoreUserRollAction extends Action<string> {
  payload: {
    roll: Array<number>;
    winnings: number;
  };
}

export const storeUser = (username: string): StoreUserAction => {
  return {
    type: USER_STORE,
    payload: {
      username
    }
  };
};

export const storeGender = (gender: string): StoreGenderAction => {
  return {
    type: GENDER_STORE,
    payload: {
      gender
    }
  };
};

export const storeUserRoll = (
  roll: Array<number>,
  winnings: number
): StoreUserRollAction => {
  return {
    type: USER_ROLL,
    payload: {
      roll,
      winnings
    }
  };
};

export const clearUser = (): Action<string> => {
  return {
    type: USER_CLEAR
  };
};
