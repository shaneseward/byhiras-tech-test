import {
  GAME_ATTACK,
  GAME_DICE_ROLLING,
  GAME_NEW,
  GAME_RESET
} from "../../constant/actions";
import { Action, AnyAction } from "redux";

export interface GameDiceRollingAction extends Action<string> {
  payload: {
    rolling: boolean;
  };
}

export const newGame = (): AnyAction => {
  return {
    type: GAME_NEW
  };
};

export const resetGame = (): AnyAction => {
  return {
    type: GAME_RESET
  };
};

export const attack = (): AnyAction => {
  return {
    type: GAME_ATTACK
  };
};

export const diceRolling = (rolling: boolean): GameDiceRollingAction => {
  return {
    type: GAME_DICE_ROLLING,
    payload: {
      rolling
    }
  };
};
