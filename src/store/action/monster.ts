import {
  MONSTER_CLEAR,
  MONSTER_ROLL,
  MONSTER_STORE
} from "../../constant/actions";
import { Action } from "redux";
import { Monster } from "../reducer/monster";

export interface StoreMonsterAction extends Action<string> {
  payload: {
    monster: Monster;
  };
}

export interface StoreMonsterRollAction extends Action<string> {
  payload: {
    roll: Array<number>;
    winnings: number;
  };
}

export const storeMonster = (monster: Monster): StoreMonsterAction => {
  return {
    type: MONSTER_STORE,
    payload: {
      monster
    }
  };
};

export const clearMonster = (): Action<string> => {
  return {
    type: MONSTER_CLEAR
  };
};

export const storeMonsterRoll = (
  roll: Array<number>,
  winnings: number
): StoreMonsterRollAction => {
  return {
    type: MONSTER_ROLL,
    payload: {
      roll,
      winnings
    }
  };
};
