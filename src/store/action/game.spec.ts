import { GAME_DICE_ROLLING, GAME_NEW } from "../../constant/actions";
import { diceRolling, GameDiceRollingAction, newGame } from "./game";

/**
 * Examples of basic (non thunk) action tests
 */
describe("game actions", () => {
  it("newGame should return correct action", () => {
    const expected = {
      type: GAME_NEW
    };

    expect(newGame()).toEqual(expected);
  });

  it("diceRolling should return correct action", () => {
    const rolling: boolean = true;
    const expected: GameDiceRollingAction = {
      type: GAME_DICE_ROLLING,
      payload: {
        rolling
      }
    };
    expect(diceRolling(rolling)).toEqual(expected);
  });
});
