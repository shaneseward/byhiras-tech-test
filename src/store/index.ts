import { AnyAction, applyMiddleware, createStore, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { reducer, State } from "./reducer";
import { gameMiddleware } from "./middleware";
import thunk from "redux-thunk";

const store: Store<State> = createStore<State, AnyAction, any, any>(
  reducer,
  composeWithDevTools(applyMiddleware(thunk, gameMiddleware))
);

export default store;
