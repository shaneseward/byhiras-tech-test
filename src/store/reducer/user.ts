import { AnyAction } from "redux";
import {
  GAME_NEW,
  GENDER_STORE,
  USER_CLEAR,
  USER_STORE,
  USER_ROLL,
  GAME_RESET
} from "../../constant/actions";
import { MAX_SCORE } from "../../constant/game";

export interface UserReducerState {
  username?: string;
  gender?: string;
  genderOptions: Array<string>;
  last: string;
  lastWinnings: number;
  score: number;
  diceRoll: Array<number>;
}

export const initialState: UserReducerState = {
  username: undefined,
  gender: undefined,
  last: "",
  lastWinnings: 0,
  score: 0,
  genderOptions: ["Male", "Female"],
  diceRoll: []
};

export function reducer(
  state: UserReducerState = initialState,
  action: AnyAction
) {
  const { type, payload } = action;
  switch (type) {
    case GAME_RESET:
      return {
        ...initialState
      };
    case USER_ROLL:
      return {
        ...state,
        diceRoll: payload.roll,
        last: payload.winnings > 0 ? "-" + payload.winnings.toString() : "",
        lastWinnings: payload.winnings,
        score: state.score - payload.winnings
      };
    case GAME_NEW:
      return {
        ...state,
        last: "",
        score: MAX_SCORE
      };
    case USER_STORE:
      return {
        ...state,
        username: payload.username
      };
    case USER_CLEAR:
      return {
        ...state,
        username: undefined
      };
    case GENDER_STORE:
      return {
        ...state,
        gender: payload.gender
      };
    default:
      return state;
  }
}
