import * as userReducer from "./user";
import * as monsterReducer from "./monster";
import * as gameReducer from "./game";
import { combineReducers } from "redux";

export interface State {
  user: userReducer.UserReducerState;
  game: gameReducer.GameReducerState;
  monster: monsterReducer.MonsterReducerState;
}

export const initialState: State = {
  user: userReducer.initialState,
  game: gameReducer.initialState,
  monster: monsterReducer.initialState
};

export const reducer = combineReducers<State>({
  user: userReducer.reducer,
  game: gameReducer.reducer,
  monster: monsterReducer.reducer
});
