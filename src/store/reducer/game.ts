import { AnyAction } from "redux";
import { GAME_DICE_ROLLING, GAME_RESET } from "../../constant/actions";

export interface GameReducerState {
  rolling: boolean;
}

export const initialState: GameReducerState = {
  rolling: false
};

export function reducer(
  state: GameReducerState = initialState,
  action: AnyAction
) {
  const { type, payload } = action;
  switch (type) {
    case GAME_RESET:
      return {
        ...initialState
      };
    case GAME_DICE_ROLLING:
      return {
        ...state,
        rolling: payload.rolling
      };
    default:
      return state;
  }
}
