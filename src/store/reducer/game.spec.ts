import { reducer, initialState, GameReducerState } from "./game";
import { GAME_DICE_ROLLING, GAME_RESET } from "../../constant/actions";

/**
 * Examples of reducer tests
 */
describe("game reducer", () => {
  it("GAME_RESET should return correct state", () => {
    const action = {
      type: GAME_RESET
    };
    const currentState: GameReducerState = {
      ...initialState,
      rolling: true
    };

    expect(reducer(currentState, action)).toEqual(initialState);
  });

  it("GAME_DICE_ROLLING should return correct state", () => {
    const action = {
      type: GAME_DICE_ROLLING,
      payload: {
        rolling: true
      }
    };
    const expectedState: GameReducerState = {
      ...initialState,
      rolling: true
    };

    expect(reducer(initialState, action)).toEqual(expectedState);
  });
});
