import { AnyAction } from "redux";
import {
  MONSTER_STORE,
  MONSTER_CLEAR,
  GAME_NEW,
  MONSTER_ROLL,
  GAME_RESET
} from "../../constant/actions";
import { MAX_SCORE } from "../../constant/game";

export interface Monster {
  path: string;
  name: string;
}

export interface MonsterReducerState {
  monster?: Monster;
  monsterOptions: Array<Monster>;
  last: string;
  lastWinnings: number;
  score: number;
  diceRoll: Array<number>;
}

export const initialState: MonsterReducerState = {
  last: "",
  lastWinnings: 0,
  score: 0,
  monster: undefined,
  diceRoll: [],
  monsterOptions: [
    {
      name: "Wolfie",
      path: "/assets/monster1.png"
    },
    {
      name: "Fangs",
      path: "/assets/monster2.png"
    },
    {
      name: "Claws",
      path: "/assets/monster3.png"
    }
  ]
};

export function reducer(
  state: MonsterReducerState = initialState,
  action: AnyAction
) {
  const { type, payload } = action;
  switch (type) {
    case GAME_RESET:
      return {
        ...initialState
      };
    case MONSTER_ROLL:
      return {
        ...state,
        diceRoll: payload.roll,
        last: payload.winnings > 0 ? "-" + payload.winnings.toString() : "",
        lastWinnings: payload.winnings,
        score: state.score - payload.winnings
      };
    case GAME_NEW:
      return {
        ...state,
        last: "",
        score: MAX_SCORE
      };
    case MONSTER_STORE:
      return {
        ...state,
        monster: payload.monster
      };
    case MONSTER_CLEAR:
      return {
        ...state,
        monster: undefined
      };
    default:
      return state;
  }
}
