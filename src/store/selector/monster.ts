import { State } from "../reducer";
import { createSelector } from "reselect";

export const monsterReducerSelector = (state: State) => state.monster;

export const monsterSelector = createSelector(
  monsterReducerSelector,
  reducer => reducer.monster
);

export const monsterOptionsSelector = createSelector(
  monsterReducerSelector,
  reducer => reducer.monsterOptions
);

export const monsterLastSelector = createSelector(
  monsterReducerSelector,
  reducer => reducer.last
);

export const monsterLastWinningsSelector = createSelector(
  monsterReducerSelector,
  reducer => reducer.lastWinnings
);

export const monsterScoreSelector = createSelector(
  monsterReducerSelector,
  reducer => reducer.score
);

export const monsterRollSelector = createSelector(
  monsterReducerSelector,
  reducer => reducer.diceRoll
);
