import { GAME_DICE_ROLLING, GAME_NEW } from "../../constant/actions";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import { diceRollingSelector } from "./game";

const mockStore = configureStore([thunk]);

/**
 * Example of selector tests
 */
describe("game selectors", () => {
  it("diceRollingSelector should return correct state", () => {
    const rolling: boolean = true;
    const initialState = {
      game: {
        rolling
      }
    };
    const store = mockStore(initialState);
    expect(diceRollingSelector(store.getState())).toEqual(rolling);
  });
});
