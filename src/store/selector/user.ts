import { State } from "../reducer";
import { createSelector } from "reselect";

export const userSelector = (state: State) => state.user;

export const usernameSelector = createSelector(
  userSelector,
  user => user.username
);

export const genderOptionsSelector = createSelector(
  userSelector,
  user => user.genderOptions
);

export const genderSelector = createSelector(userSelector, user => user.gender);

export const userPathSelector = createSelector(userSelector, user =>
  user.gender ? `/assets/${user.gender.toLowerCase()}.png` : undefined
);

export const userLastSelector = createSelector(userSelector, user => user.last);

export const userLastWinningsSelector = createSelector(
  userSelector,
  user => user.lastWinnings
);

export const userScoreSelector = createSelector(
  userSelector,
  user => user.score
);

export const userRollSelector = createSelector(
  userSelector,
  user => user.diceRoll
);
