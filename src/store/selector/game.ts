import { State } from "../reducer";
import { createSelector } from "reselect";

export const gameSelector = (state: State) => state.game;

export const diceRollingSelector = createSelector(
  gameSelector,
  game => game.rolling
);
