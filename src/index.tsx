import React from "react";
import ReactDom from "react-dom";
import { Provider } from "react-redux";
import { ThemeProvider } from "@material-ui/core/styles";
import { CssBaseline } from "@material-ui/core";
import store from "./store";
import AppTheme from "./theme/AppTheme";
import "./index.scss";
import Welcome from "./screens/Welcome/Welcome";
import Battle from "./screens/Battle/Battle";
import { ParentRouteProps } from "./components/ParentRoute/ParentRoute";
import App from "./App";
import GameComplete from "./components/GameComplete/GameComplete";

const routes: Array<ParentRouteProps> = [
  {
    path: "/complete",
    exact: true,
    component: GameComplete
  },
  {
    path: "/play",
    exact: true,
    component: Battle
  },
  {
    path: "/",
    exact: false,
    component: Welcome
  }
];

const AppShell = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={AppTheme}>
        <CssBaseline />
        <App routes={routes} />
      </ThemeProvider>
    </Provider>
  );
};

ReactDom.render(<AppShell />, document.getElementById("root"));
