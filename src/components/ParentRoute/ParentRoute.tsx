import React from "react";
import { Route } from "react-router-dom";

export interface ParentRouteProps {
  path: string;
  component: React.FC<any>;
  exact: boolean;
  routes?: Array<ParentRouteProps>;
}

const ParentRoute: React.FC<ParentRouteProps> = (props: ParentRouteProps) => {
  const { path, exact, routes } = props;
  const RouteComponent: React.FC<any> = props.component;
  return (
    <Route
      path={path}
      exact={exact}
      render={renderProps => (
        <RouteComponent {...renderProps} routes={routes} />
      )}
    />
  );
};

export default ParentRoute;
