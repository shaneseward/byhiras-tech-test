import React from "react";
import styles from "./HealthBarStyles.module.scss";

interface HealthBarProps {
  variant: "primary" | "secondary";
  score: number;
  last: string;
}

const HealthBar = (props: HealthBarProps) => {
  return (
    <div className={`${styles.root} ${styles[props.variant]}`}>
      <div className={styles.last}>{props.last}</div>
      <div className={styles.barContainer}>
        <div
          className={styles.bar}
          style={{
            height: `${props.score}%`
          }}
        ></div>
      </div>
      <div className={styles.score}>
        {props.score > 0 && props.score.toString()}
      </div>
    </div>
  );
};

export default HealthBar;
