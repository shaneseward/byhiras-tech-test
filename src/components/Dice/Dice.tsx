import React from "react";
import styles from "./DiceStyles.module.scss";
import { useSelector } from "react-redux";
import { State } from "../../store/reducer";
import { diceRollingSelector } from "../../store/selector/game";

interface DiceProps {
  roll: Array<number>;
}

const diceClass: Array<string> = ["one", "two", "three", "four", "five", "six"];

const Dice = (props: DiceProps) => {
  const dice1: string = props.roll.length
    ? diceClass[props.roll[0] - 1]
    : "disabled";
  const dice2: string = props.roll.length
    ? diceClass[props.roll[1] - 1]
    : "disabled";
  const rolling = useSelector((state: State) => diceRollingSelector(state));

  return (
    <div className={`${styles.root} ${rolling && styles.rolling}`}>
      <div className={`${styles.dice} ${styles[dice1]}`} />
      <div className={`${styles.dice} ${styles[dice2]}`} />
    </div>
  );
};

export default Dice;
