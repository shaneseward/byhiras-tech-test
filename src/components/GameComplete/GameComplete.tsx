import React, { useEffect } from "react";
import styles from "./GameCompleteStyles.module.scss";
import { useHistory } from "react-router-dom";
import { Button, FormControl } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { State } from "../../store/reducer";
import {
  monsterScoreSelector,
  monsterSelector
} from "../../store/selector/monster";
import {
  genderSelector,
  usernameSelector,
  userScoreSelector
} from "../../store/selector/user";
import { resetGame } from "../../store/action/game";

interface GameCompleteProps {}

const GameComplete = (props: GameCompleteProps) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const monster = useSelector((state: State) => monsterSelector(state));
  const monsterName = monster ? monster.name : "";
  const username = useSelector((state: State) => usernameSelector(state));
  const gender = useSelector((state: State) => genderSelector(state));
  const userScore = useSelector((state: State) => userScoreSelector(state));
  const monsterScore = useSelector((state: State) =>
    monsterScoreSelector(state)
  );
  const winnerName = monsterScore < userScore ? monsterName : username;
  const message = `${winnerName} is the winner!`;

  const handleReplay = () => {
    dispatch(resetGame());
    history.push("/");
  };

  useEffect(() => {
    if (
      !gender ||
      !username ||
      !monster ||
      (userScore > 0 && monsterScore > 0)
    ) {
      dispatch(resetGame());
      history.push("/");
    }
  }, [dispatch, history, monster, username, gender, userScore, monsterScore]);

  return (
    <div className={styles.root}>
      <div className={styles.form}>
        <div className={styles.messageContainer}>
          <div className={styles.message}>{message}</div>
        </div>
        <FormControl className={styles.formControl}>
          <Button onClick={handleReplay} variant="contained" color="primary">
            Replay
          </Button>
        </FormControl>
      </div>
    </div>
  );
};

export default GameComplete;
