import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import ParentRoute, { ParentRouteProps } from "../ParentRoute/ParentRoute";

interface RouterProps {
  routes: Array<ParentRouteProps>;
}

const Router: React.FC<RouterProps> = (props: RouterProps) => {
  const renderRoutes = (routes: Array<ParentRouteProps>) => {
    return (
      <Switch>
        {routes.map((route: ParentRouteProps) => (
          <ParentRoute key={route.path} {...route} />
        ))}
      </Switch>
    );
  };

  return <BrowserRouter>{renderRoutes(props.routes)}</BrowserRouter>;
};

export default Router;
