import React from "react";
import styles from "./HeaderStyles.module.scss";

const Header: React.FC<any> = () => {
  return (
    <div className={styles.root}>
      <img alt="Title" className={styles.titleImg} src="/assets/title.png" />
    </div>
  );
};

export default Header;
