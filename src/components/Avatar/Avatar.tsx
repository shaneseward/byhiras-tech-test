import React from "react";
import styles from "./AvatarStyles.module.scss";

interface AvatarProps {
  path?: string;
}

const Avatar = (props: AvatarProps) => {
  return (
    <div className={styles.root}>
      {props.path && <img alt="Avatar" src={props.path} />}
    </div>
  );
};

export default Avatar;
