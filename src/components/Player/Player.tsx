import React from "react";
import Avatar from "../Avatar/Avatar";
import HealthBar from "../HealthBar/HealthBar";
import Dice from "../Dice/Dice";
import styles from "./PlayerStyles.module.scss";
import { useSelector } from "react-redux";
import { State } from "../../store/reducer";
import {
  userLastSelector,
  usernameSelector,
  userPathSelector,
  userRollSelector,
  userScoreSelector
} from "../../store/selector/user";
import PlayerName from "../PlayerName/PlayerName";

interface PlayerProps {}

const Player = (props: PlayerProps) => {
  const username = useSelector((state: State) => usernameSelector(state));
  const path = useSelector((state: State) => userPathSelector(state));
  const last = useSelector((state: State) => userLastSelector(state));
  const score = useSelector((state: State) => userScoreSelector(state));
  const roll = useSelector((state: State) => userRollSelector(state));

  return (
    <div className={styles.root}>
      <div className={styles.details}>
        <Avatar path={path} />
        <HealthBar variant="primary" last={last} score={score} />
        <Dice roll={roll} />
      </div>
      <div className={styles.name}>
        <PlayerName name={username} variant="primary" />
      </div>
    </div>
  );
};

export default Player;
