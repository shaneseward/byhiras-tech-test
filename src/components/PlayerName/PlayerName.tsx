import React from "react";
import styles from "./PlayerNameStyles.module.scss";

interface PlayerNameProps {
  name?: string;
  variant: "primary" | "secondary";
}

const PlayerName = (props: PlayerNameProps) => {
  return (
    <div className={`${styles.root} ${styles[props.variant]}`}>
      {props.name}
    </div>
  );
};

export default PlayerName;
