import React from "react";
import styles from "./PlayerInputStyles.module.scss";
import { useHistory } from "react-router-dom";
import {
  TextField,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Button
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { State } from "../../store/reducer";
import {
  genderOptionsSelector,
  genderSelector,
  usernameSelector
} from "../../store/selector/user";
import { clearUser, storeGender, storeUser } from "../../store/action/user";
import {
  monsterOptionsSelector,
  monsterSelector
} from "../../store/selector/monster";
import { storeMonster } from "../../store/action/monster";
import { newGame } from "../../store/action/game";

interface PlayerInputProps {}

const PlayerInput = (props: PlayerInputProps) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const username = useSelector((state: State) => usernameSelector(state));
  const genderOptions = useSelector((state: State) =>
    genderOptionsSelector(state)
  );
  const gender = useSelector((state: State) => genderSelector(state));
  const monsterOptions = useSelector((state: State) =>
    monsterOptionsSelector(state)
  );
  const monster = useSelector((state: State) => monsterSelector(state));
  const disableButton = !gender || !username || !monster;

  const handleGenderChange = (event: any) => {
    if (event.target.value) {
      dispatch(storeGender(event.target.value));
    }
  };

  const handleNameChange = (event: any) => {
    if (event.target.value) {
      dispatch(storeUser(event.target.value));
    } else {
      dispatch(clearUser());
    }
  };

  const handleMonsterChange = (event: any) => {
    console.log(event.target.value);
    if (event.target.value) {
      const monster = monsterOptions.find(
        option => option.name === event.target.value
      );
      if (monster) dispatch(storeMonster(monster));
    }
  };

  const handlePlay = () => {
    dispatch(newGame());
    history.push("/play");
  };

  return (
    <div className={styles.root}>
      <div className={styles.form}>
        <FormControl className={styles.formControl}>
          <TextField
            id="filled-password-input"
            label="Name"
            type="text"
            value={username ? username : ""}
            variant="outlined"
            onChange={handleNameChange}
            inputProps={{
              maxLength: 20
            }}
          />
        </FormControl>
        <FormControl className={styles.formControl} variant="outlined">
          <InputLabel id="gender-input-label">Gender</InputLabel>
          <Select
            labelId="gender-input-label"
            id="gender-input"
            value={gender ? gender : ""}
            onChange={handleGenderChange}
            labelWidth={55}
          >
            {genderOptions.map(option => (
              <MenuItem key={option} value={option}>
                {option}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={styles.formControl} variant="outlined">
          <InputLabel id="monster-input-label">Select Monster</InputLabel>
          <Select
            labelId="monster-input-label"
            id="monster-input"
            value={monster ? monster.name : ""}
            onChange={handleMonsterChange}
            labelWidth={110}
          >
            {monsterOptions.map(option => (
              <MenuItem key={option.name} value={option.name}>
                {option.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={styles.formControl}>
          <Button
            disabled={disableButton}
            onClick={handlePlay}
            variant="contained"
            color="primary"
          >
            Play
          </Button>
        </FormControl>
      </div>
    </div>
  );
};

export default PlayerInput;
