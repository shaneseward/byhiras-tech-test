import React from "react";
import styles from "./FooterStyles.module.scss";

const Footer: React.FC<any> = () => {
  return (
    <div className={styles.root}>
      <span>Shane Seward:</span>
      <span>
        <a href="http://http://www.shaneseward.co.uk/">
          http://www.shaneseward.co.uk/
        </a>
      </span>
      <span>
        <a href="mailto:shane@shaneseward.co.uk">shane@shaneseward.co.uk</a>
      </span>
    </div>
  );
};

export default Footer;
