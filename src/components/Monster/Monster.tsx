import React from "react";
import Avatar from "../Avatar/Avatar";
import HealthBar from "../HealthBar/HealthBar";
import Dice from "../Dice/Dice";
import styles from "./MonsterStyles.module.scss";
import { useSelector } from "react-redux";
import { State } from "../../store/reducer";
import {
  monsterLastSelector,
  monsterRollSelector,
  monsterScoreSelector,
  monsterSelector
} from "../../store/selector/monster";
import PlayerName from "../PlayerName/PlayerName";

interface MonsterProps {}

const Monster = (props: MonsterProps) => {
  const monster = useSelector((state: State) => monsterSelector(state));
  const path = monster ? monster.path : undefined;
  const name = monster ? monster.name : undefined;
  const last = useSelector((state: State) => monsterLastSelector(state));
  const score = useSelector((state: State) => monsterScoreSelector(state));
  const roll = useSelector((state: State) => monsterRollSelector(state));

  return (
    <div className={styles.root}>
      <div className={styles.details}>
        <Dice roll={roll} />
        <HealthBar variant="secondary" last={last} score={score} />
        <Avatar path={path} />
      </div>
      <div className={styles.name}>
        <PlayerName name={name} variant="secondary" />
      </div>
    </div>
  );
};

export default Monster;
