import React, { useEffect } from "react";
import styles from "./GameDetailsStyles.module.scss";
import { useHistory } from "react-router-dom";
import { Button, FormControl } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { State } from "../../store/reducer";
import {
  monsterLastWinningsSelector,
  monsterScoreSelector,
  monsterSelector
} from "../../store/selector/monster";
import {
  genderSelector,
  userLastWinningsSelector,
  usernameSelector,
  userScoreSelector
} from "../../store/selector/user";
import { diceRollingSelector } from "../../store/selector/game";
import { attack } from "../../store/action/game";

interface GameDetailsProps {}

const GameDetails = (props: GameDetailsProps) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const monster = useSelector((state: State) => monsterSelector(state));
  const monsterName = monster ? monster.name : "";
  const username = useSelector((state: State) => usernameSelector(state));
  const gender = useSelector((state: State) => genderSelector(state));
  const disableButton = useSelector((state: State) =>
    diceRollingSelector(state)
  );
  const userWinnings = useSelector((state: State) =>
    userLastWinningsSelector(state)
  );
  const monsterWinnings = useSelector((state: State) =>
    monsterLastWinningsSelector(state)
  );
  const userScore = useSelector((state: State) => userScoreSelector(state));
  const monsterScore = useSelector((state: State) =>
    monsterScoreSelector(state)
  );
  const winnerName = userWinnings > monsterWinnings ? username : monsterName;
  const winnings =
    userWinnings > monsterWinnings ? userWinnings : monsterWinnings;
  const message = `${winnerName} hit for ${winnings}`;

  const handleAttack = () => {
    dispatch(attack());
  };

  useEffect(() => {
    if (!gender || !username || !monster) {
      history.push("/");
    }
    if (userScore <= 0 && monsterScore > 0) {
      history.push("/complete");
    }
    if (monsterScore <= 0 && userScore > 0) {
      history.push("/complete");
    }
  }, [history, monster, username, gender, userScore, monsterScore]);

  return (
    <div className={styles.root}>
      <div className={styles.form}>
        <div className={styles.messageContainer}>
          {winnings === 0 && !disableButton && (
            <div className={styles.message}>
              Click 'Attack' and start the Battle!
            </div>
          )}
          {winnings > 0 && !disableButton && (
            <div className={styles.message}>{message}</div>
          )}
        </div>
        <FormControl className={styles.formControl}>
          <Button
            disabled={disableButton}
            onClick={handleAttack}
            variant="contained"
            color="primary"
          >
            Attack
          </Button>
        </FormControl>
      </div>
    </div>
  );
};

export default GameDetails;
