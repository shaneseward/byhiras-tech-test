import React from "react";
import { ParentRouteProps } from "./components/ParentRoute/ParentRoute";
import styles from "./AppStyles.module.scss";
import Router from "./components/Router/Router";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

interface AppProps {
  routes: Array<ParentRouteProps>;
}

const App: React.FC<AppProps> = (props: AppProps) => {
  const { routes } = props;

  return (
    <div className={styles.root}>
      <div className={styles.container}>
        <Header />
        <Router routes={routes} />
        <Footer />
      </div>
    </div>
  );
};

export default App;
