

## Byhiras 'Battle Simulator' technical test

### Stack Used

React (Hooks) <br />
Redux (App State) <br />
Reselect (Memoize State) <br />
NodeSass (Styles) <br />
Material-UI (UI Components) <br />
Jest, redux-mock-store, react-test-renderer (Testing & Mocking) <br />

### Tests

Unfortunately due to time constraints I was unable to offer full test coverage. <br />

Instead I have given a sample of testing various aspects of the React and Redux framework: <br />

<ul>
    <li>src/store/action/game.spec.ts</li>
    <li>src/store/middleware/game.spec.ts</li>
    <li>src/store/reducer/game.spec.ts</li>
    <li>src/store/selector/game.spec.ts</li>
    <li>src/screens/Battle/Battle.spec.ts</li>
</ul>


### Author

Shane Seward <br />
shane@shaneseward.co.uk <br />
www.shaneseward.co.uk <br />
